package aws;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Paths;

@SpringBootApplication
public class S3App {
    public static void main(String[] args) {
        SpringApplication.run(S3App.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

@RefreshScope
@RestController
class MessageRestController {

    @Autowired
    RestTemplate restTemplate;

    @Value("${bucket: no}")
    private String bucket;

    @Value("${accesskey: no}")
    private String accesskey;

    @Value("${secretkey: no}")
    private String secretkey;

    @RequestMapping("/test")
    public String test() {
        return "s3";
    }

    @RequestMapping("/consul")
    public String getConsul(){
        String resp = restTemplate.getForObject("http://cloud-config-service/s3/default", String.class);
        System.out.println(resp);
        return resp;
    }

    @RequestMapping("/upload")
    String uploadToAws() {

        String bucket_name = this.bucket;
        String file_path = "pom.xml";
        String key_name = Paths.get(file_path).getFileName().toString();

        System.out.format("Uploading %s to S3 bucket %s...\n", file_path, bucket_name);

//        final AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accesskey, secretkey);
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.US_EAST_2)
                .build();


        try {
            s3.putObject(bucket_name, key_name, file_path);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            return e.getErrorMessage();
        }
        System.out.println("Done!");

        return "Done!";
    }
}

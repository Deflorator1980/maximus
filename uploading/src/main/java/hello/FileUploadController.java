package hello;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.AmazonServiceException;


import hello.storage.StorageFileNotFoundException;
import hello.storage.StorageService;

@RefreshScope
@Controller
@RestController
public class FileUploadController {

    @Value("${bucket: no}")
    private String bucket;

    @Value("${accesskey: no}")
    private String accesskey;

    @Value("${secretkey: no}")
    private String secretkey;

    private final StorageService storageService;

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/test")
    public String test() {

       return "test";
    }

    @RequestMapping("/consul")
    public String getConsul(){
//        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.exchange("http://cloud-config-service/a-bootiful-client/default",
//        String response = restTemplate.exchange("http://localhost:8888/a-bootiful-client/default",
                HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}).getBody();
        return response;
    }

    @GetMapping("/")
    public String listUploadedFiles(Model model) throws IOException {

        model.addAttribute("files", storageService.loadAll().map(
                path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
                        "serveFile", path.getFileName().toString()).build().toString())
                .collect(Collectors.toList()));

        return "uploadForm";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) {

        storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        String file_path = "upload-dir/"+file.getOriginalFilename();
        String key_name = Paths.get(file_path).getFileName().toString();
        String bucket_name = this.bucket;
//        String bucket_name = "wave15";
        System.out.format("Uploading %s to S3 bucket %s...\n", file_path, bucket_name);

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accesskey, secretkey);
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.US_EAST_2)
                .build();
//        final AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();

        try {
            s3.putObject(bucket_name, key_name, file_path);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            return e.getErrorMessage();
        }
        System.out.println("Done!");

        return "redirect:/";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}

